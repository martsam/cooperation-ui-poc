'use strict';

angular.module('cooperationApp')
  .controller('ProducerCtrl',
    function ($scope, $q, CooperationService) {
      CooperationService.getConnectionPoints().then(function (data) {
        $scope.connectionPoints = data;
      });

      $scope.selectedServiceProducer = {};
      $scope.serviceProducers = [];

      $scope.$watch('selectedConnectionPoint', function (connectionPoint) {
        if (connectionPoint && connectionPoint.id) {
          $scope.selectedServiceProducer = {};
          $scope.cooperations = [];
          console.log('connection point selected', connectionPoint);
          CooperationService.getServiceProducers(connectionPoint.id).then(function (data) {
            $scope.serviceProducers = data;
            $scope.filteredServiceProducers = data;
          });
        }
      });

      $scope.$watch('selectedServiceProducer.selected', function (serviceProducer) {
        if (serviceProducer && serviceProducer.id) {
          console.log('service producer selected', serviceProducer);
          CooperationService.getServiceProductionsForProducer($scope.selectedConnectionPoint.id, serviceProducer.id).then(function(data){
            $scope.productions = data;
          });
        }
      });

      $scope.filteredServiceProducers = [];

      $scope.filterServiceProducers = function (filterBy) {
        if (!_.isUndefined(filterBy)) {
          var filterByLower = filterBy.toLowerCase();
          $scope.filteredServiceProducers = _.filter($scope.serviceProducers, function (serviceProducer) {
            var hsaIdLower = serviceProducer.hsaId.toLowerCase();
            var descriptionLower = serviceProducer.description.toLowerCase();
            return hsaIdLower.indexOf(filterByLower) > -1 || descriptionLower.indexOf(filterByLower) > -1;
          });
        } else {
          $scope.filteredServiceProducers = $scope.serviceProducers;
        }
      };

    });
