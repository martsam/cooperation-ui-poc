'use strict';

angular
  .module('cooperationApp', [
    'services.config',
    'ui.router',
    'ui.bootstrap',
    'ui.select',
    'ngSanitize'
  ])
  .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/consumer');

    // Now set up the states
    $stateProvider
      .state('consumer', {
        url: '/consumer',
        templateUrl: 'consumer/consumer.html',
        controller: 'ConsumerCtrl'
      })
      .state('producer', {
        url: '/producer',
        templateUrl: 'producer/producer.html',
        controller: 'ProducerCtrl'
      })
      .state('serviceContract', {
        url: '/serviceContract',
        templateUrl: 'serviceContract/serviceContract.html',
        controller: 'ServiceContractCtrl'
      })
      ;
  }]);
