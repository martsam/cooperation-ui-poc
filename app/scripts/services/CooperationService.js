'use strict';

angular.module('cooperationApp')
  .factory('CooperationService', function ($q, $http, configuration) {

    var getConnectionPoints = function () {
      var deferred = $q.defer();
      $http.get(configuration.basePath + '/connectionPoints')
        .success(function (data) {
          deferred.resolve(data);
        }).error(function () {
          console.log('no error handling implemented');
          deferred.reject();
        });
      return deferred.promise;
    };

    var getServiceConsumers = function (connectionPointId) {
      var deferred = $q.defer();
      $http.get(configuration.basePath + '/serviceConsumers', {
        params: {
          connectionPointId: connectionPointId
        }
      })
        .success(function (data) {
          console.log('service consumers for connectionPointId[' + connectionPointId + ']', data);
          deferred.resolve(data);
        }).error(function () {
          console.log('no error handling implemented');
          deferred.reject();
        });
      return deferred.promise;
    };

    var getServiceProducers = function (connectionPointId) {
      var deferred = $q.defer();
      $http.get(configuration.basePath + '/serviceProducers', {
          params: {
            connectionPointId: connectionPointId,
            include: 'id,description'
          }
        })
        .success(function (data) {
          deferred.resolve(data);
        }).error(function () {
        console.log('no error handling implemented');
        deferred.reject();
      });
      return deferred.promise;
    };

    var getCooperationsForConsumer = function(connectionPointId, serviceConsumerId) { //TODO: rename
      return _getCooperations({
        connectionPointId: connectionPointId,
        serviceConsumerId: serviceConsumerId,
        include: 'logicalAddress,serviceContract'
      });
    };

    var getCooperationsForContract = function(connectionPointId, serviceContractId) { //TODO: rename
      return _getCooperations({
        connectionPointId: connectionPointId,
        serviceContractId: serviceContractId,
        include: 'logicalAddress,serviceConsumer'
      });
    };

    var getServiceProductionsForProducer = function(connectionPointId, serviceProducerId) {
      return _getServiceProductions({
        connectionPointId: connectionPointId,
        serviceProducerId: serviceProducerId,
        include: 'logicalAddress,serviceContract'
      });
    };

    var getServiceProductionsForContract = function(connectionPointId, serviceContractId) {
      return _getServiceProductions({
        connectionPointId: connectionPointId,
        serviceContractId: serviceContractId,
        include: 'id,logicalAddress,serviceProducer'
      });
    };

    var getServiceContracts = function(connectionPointId) {
      var deferred = $q.defer();
      $http.get(configuration.basePath + '/serviceContracts', {
        params: {
          connectionPointId: connectionPointId
        }
      })
        .success(function (data) {
          deferred.resolve(data);
        }).error(function () {
        console.log('no error handling implemented');
        deferred.reject();
      });
      return deferred.promise;
    };

    var _getCooperations = function(params) {
      var deferred = $q.defer();
      $http.get(configuration.basePath + '/cooperations', {
          params: params
        })
        .success(function (data) {
          deferred.resolve(data);
        }).error(function () {
        console.log('no error handling implemented');
        deferred.reject();
      });
      return deferred.promise;
    };

    var _getServiceProductions = function (params) {
      var deferred = $q.defer();
      $http.get(configuration.basePath + '/serviceProductions', {
          params: params
        })
        .success(function (data) {
          deferred.resolve(data);
        }).error(function () {
        console.log('no error handling implemented');
        deferred.reject();
      });
      return deferred.promise;
    };

    return {
      getConnectionPoints: getConnectionPoints,
      getServiceConsumers: getServiceConsumers,
      getServiceProducers: getServiceProducers,
      getCooperationsForConsumer: getCooperationsForConsumer,
      getCooperationsForContract: getCooperationsForContract,
      getServiceProductionsForProducer: getServiceProductionsForProducer,
      getServiceProductionsForContract: getServiceProductionsForContract,
      getServiceContracts: getServiceContracts
    };


  });
