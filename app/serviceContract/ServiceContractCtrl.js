'use strict';

angular.module('cooperationApp')
  .controller('ServiceContractCtrl',
    function ($scope, $q, CooperationService) {
      CooperationService.getConnectionPoints().then(function (data) {
        $scope.connectionPoints = data;
      });

      $scope.selectedServiceContract = {};
      $scope.serviceContracts = [];

      $scope.$watch('selectedConnectionPoint', function (connectionPoint) {
        if (connectionPoint && connectionPoint.id) {
          $scope.selectedServiceContract = {};
          $scope.cooperations = [];
          $scope.productions = [];
          console.log('connection point selected', connectionPoint);
          CooperationService.getServiceContracts(connectionPoint.id).then(function (data) {
            $scope.serviceContracts = data;
            $scope.filteredServiceContracts = data;
          });
        }
      });

      $scope.$watch('selectedServiceContract.selected', function (serviceContract) {
        if (serviceContract && serviceContract.id) {
          $scope.cooperations = [];
          $scope.productions = [];
          console.log('service contract selected', serviceContract);
          CooperationService.getCooperationsForContract($scope.selectedConnectionPoint.id, serviceContract.id).then(function (data) {
            $scope.cooperations = data;
          });
          CooperationService.getServiceProductionsForContract($scope.selectedConnectionPoint.id, serviceContract.id).then(function (data) {
            $scope.productions = data;
          });
        }
      });

      $scope.filteredServiceContracts = [];

      $scope.filterServiceContracts = function (filterBy) {
        if (!_.isUndefined(filterBy)) {
          var filterByLower = filterBy.toLowerCase();
          $scope.filteredServiceContracts = _.filter($scope.serviceContracts, function (serviceContract) {
            var name = serviceContract.name.toLowerCase();
            var namespace = serviceContract.namespace.toLowerCase();
            return name.indexOf(filterByLower) > -1 || namespace.indexOf(filterByLower) > -1;
          });
        } else {
          $scope.filteredServiceContracts = $scope.serviceContracts;
        }
      };

    });
